package ge.edu.btu.calculatorgroup11giorgisanodze

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        buttonSeven.setOnClickListener {
            addNumber(buttonSeven)
        }
        buttonFour.setOnClickListener {
            addNumber(buttonFour)
        }
        buttonOne.setOnClickListener {
            addNumber(buttonOne)
        }
        buttonDot.setOnClickListener {

        }
        buttonEight.setOnClickListener {
            addNumber(buttonEight)
        }
        buttonFive.setOnClickListener {
            addNumber(buttonFive)
        }
        buttonTwo.setOnClickListener {
            addNumber(buttonTwo)
        }
        buttonNull.setOnClickListener {
            addNumber(buttonNull)
        }
        buttonNine.setOnClickListener {
            addNumber(buttonNine)
        }
        buttonSix.setOnClickListener {
            addNumber(buttonSix)
        }
        buttonThree.setOnClickListener {
            addNumber(buttonThree)
        }
        buttonEquals.setOnClickListener {

        }
        buttonDel.setOnClickListener {
            textView.setText("")
        }
        buttonDivide.setOnClickListener {

        }
        buttonMultiple.setOnClickListener {

        }
        buttonMinus.setOnClickListener {

        }
        buttonPlus.setOnClickListener {

        }
    }

    private fun addNumber(button: Button) {
        textView.text = button.text.toString() + button.text.toString()

    }

}
